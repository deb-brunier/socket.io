// S'assure que le DOM est entièrement chargé avant d'exécuter le code
document.addEventListener('DOMContentLoaded', () => {
    const socket = io(); // Connexion au serveur WebSocket via Socket.io
    let name;
    let textarea = document.querySelector('#textarea');
    let messageArea = document.querySelector('.message__area');

    // Boucle pour la saisie du pseudo obligatoire
    do {
        name = prompt('Merci de saisir votre pseudo : ');
        // supp les espaces au début et à la fin du pseudo, garanti que le pseudo n'est pas constitué que d'espaces
        if (name) {
            name = name.trim();
        }
    } while (!name);
    textarea.focus(); // Positionne le curseur dans le textarea une fois le pseudo saisi
    // Ajoute un gestionnaire d'événements pour détecter la touche 'Enter' dans le textarea
    textarea.addEventListener('keyup', (e) => {
        if (e.key === 'Enter') {
            let message = e.target.value.trim();
            if (message) {
                sendMessage(message);
            }
        }
    });

    // Obtient l'heure actuelle formatée
    function getCurrentTime() {
        const now = new Date();
        return now.getHours().toString().padStart(2, '0') + ':' +
            now.getMinutes().toString().padStart(2, '0');
    }

    // Envoyer des messages
    function sendMessage(message) {
        let msg = {
            user: name,
            message: message,
            time: getCurrentTime() // Ajoute l'heure actuelle au message
        };

        appendMessage(msg, 'outgoing');// Ajoute le msg à la zone de chat en tant que message sortant
        textarea.value = ''; // Réinitialise le champ de saisie du message
        scrollToBottom(); // Fait défiler la zone de message vers le bas
        socket.emit('message', msg); // Envoie le message au serveur via WebSocket
    }

    // Ajoute le message à la zone de chat
    function appendMessage(msg, type) {
        let mainDiv = document.createElement('div');
        let className = type;
        mainDiv.classList.add(className, 'message');
        // Construit le HTML pour le message
        let markup = `
            <h4>${msg.user}</h4>
            <p>${msg.message}</p>
            <span class="message__time">${msg.time}</span>
        `;
        mainDiv.innerHTML = markup;
        messageArea.appendChild(mainDiv);
    }

    // Gestionnaire d'événements pour recevoir des messages du serveur
    socket.on('message', (msg) => {
        appendMessage(msg, 'incoming'); //
        scrollToBottom();
    });

    // Fonction pour faire défiler automatiquement la zone de chat vers le dernier message
    function scrollToBottom() {
        messageArea.scrollTop = messageArea.scrollHeight;
    }
});
