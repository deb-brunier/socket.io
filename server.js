const express = require('express') // Import express
const app = express() // Création de l'application express
const http = require('http').createServer(app) //

const PORT = process.env.PORT || 3000 //  Port que le serveur va écouter

// Démarrage du serveur HTTP sur le port
http.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`)
})

// Configuration de l'app depuis le répertoire 'public'
app.use(express.static(__dirname + '/public'))

// Définition d'une route GET pour la racine ('/') pour servir le fichier 'index.html'
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

// Importation de Socket.IO et attachement au serveur HTTP
const io = require('socket.io')(http)

// Écoute pour les nouvelles connexions WebSocket
io.on('connection', (socket) => {
    console.log('Connecté...')
    // Gestion de l'événement 'message' reçu d'un client
    socket.on('message', (msg) => {
        socket.broadcast.emit('message', msg)// Diffusion du message à tous les clients connectés sauf l'expéditeur
    })

})