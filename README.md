# Socket.io chat
Ce projet est un chat en websocket.<br>
Il permet de communiquer en temps réel avec d'autres utilisateurs.

## Table des matières
- [Socket.io chat](#socketio-chat)
  - [Table des matières](#table-des-matières)
  - [Prérequis](#prérequis)
  - [Installation](#installation)
      - [Clonez le projet depuis le dépôt GitLab :](#clonez-le-projet-depuis-le-dépôt-gitlab-)
      - [Installez les dépendances :](#installez-les-dépendances-)
  - [Lancement du projet](#lancement-du-projet)
  - [Auteur](#auteur)
  - [Licence](#licence)

## Prérequis
Avant d'installer et d'utiliser ce projet, assurez-vous d'avoir les éléments suivants :
- node.js - https://nodejs.org/en/

## Installation

Suivez les étapes ci-dessous pour installer et configurer le projet :

#### Clonez le projet depuis le dépôt GitLab :

```shell
$ git clone https://gitlab.com/deb-brunier/socket.io.git

$ cd socket.io
```
#### Installez les dépendances :
```shell
$ npm i
```

## Lancement du projet
```shell
$ npm start
```
URL :  http://localhost:3000
Ouvrir plusieurs onglets pour tester le chat.

## Auteur
Ce projet a été développé par [_Déb_](https://gitlab.com/deb-brunier)


## Licence
Ce projet est sous licence [MIT](LICENSE). Vous pouvez consulter le fichier [LICENSE](LICENSE) pour plus de détails.